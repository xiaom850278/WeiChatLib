<?php 
namespace WeiChatLib;

/**
* 
*/
class MegReceiveAddtion implements IMegAddtion
{
	private $isTrunToService=false;
	/**
	 * 可以转发到客服的限制列表
	 * @var array
	 */
	private $canTruntoService=array("text","image");
	//这里可以通过判断消息类型来对消息进行不同的后续处理
	public function addtionAction($messge){
		// $this->turnToService($messge);
		//目前先自动回复
		$handle=Configuration::getInstance()->getConfig("handleLinkList");
		$handle=new $handle[0]();
		// $handle=new TextMegHandle();
		$handle->nextHandleToSend($messge,"haha");
		//.....其他代码操作，比如写入数据库等操作
	}
	
	/**
	 * 转接人工
	 * @param  [type] $messge [description]
	 * @return [type]         [description]
	 */
	public function turnToService($messge){
		if($this->isTrunToService){
			exit;
		}
	}
	/**
	 * 是否转接到人工客服
	 * @param  [type]  $bool [description]
	 * @return boolean       [description]
	 */
	public function _setTrunToService($bool){
		$this->isTrunToService=$bool;
	}


}