<?php 
namespace WeiChatLib;
/**
* 
*/
abstract class MegHandle
{
	private $LinkListHandle;
	/**
	 * 
	 * @param string $LinkList [如果转换成了事件链条的话这个要设置成时间链条的配置key]
	 */
	public function __construct($LinkList="handleLinkList"){
		$this->LinkListHandle=Configuration::getInstance()->getConfig($LinkList);
	}
	/**
	 * 调用下一个handle的执行方法
	 *   __CLASS__ 获取当前类名
  	*	__FUNCTION__ 当前函数名（confirm）
  	*	__METHOD__ 当前方法名 （bankcard::confirm）
	 * @param [type] $now      [现在的类]
	 * @param [type] $function [调用的类]
	 */
	public function HandleNext($now,$function,$option){
		$index=array_search($now, $this->LinkListHandle);
		//如果handle链没有了就不继续往下面执行了
		if(!$index+2>count($this->LinkListHandle)){
			return;
		}
		$classname=$this->LinkListHandle[$index+1]();
		if(!in_array($classname,array_values(Directory::_getAutoloadFileMap()))){
			throw new Exception("Handle 不再全局类列表中", 1);
		}
		$class=new $this->LinkListHandle[$index+1]();
		if(!is_array($option)){
			return false;
		}
		return call_user_func_array(array($class,$function),$option);
	}
}
