<?php 
namespace WeiChatLib;
/**
*开始调用事件交互链条
*/
class MegHandleStart
{
	private $startHandel;
	public function __construct(){
		$startHandelConfig=Configuration::getInstance()->getConfig("handleLinkList");
		$this->startHandel=new $startHandelConfig[0]();//实例化消息处理链条的开头
		// $this->startHandel=new TextMegHandle();//实例化消息处理链条的开头
	}
	public function CreateSendHandleStart($messge,$RequestContent){
		$this->startHandel->nextHandleToSend($messge,$RequestContent);
	}
	public function CreateReceiveHandleStart($messge){
		$this->startHandel->nextHandleToReceive($messge);
	}
}
