<?php 
namespace WeiChatLib;

/**
* 工具创建工厂
*/
class ToolFactory
{
	/**
	 * 创建http工具
	 * @return [type] [description]
	 */
	public static function createHttpTool(){
		return  new HttpCurl();
	}
	/**
	 * 创建发送服务器数据的工具
	 针对消息
	 * @return [type] [description]
	 */
	public static function createServiceDataSendTool(){
		return new ServiceDataSend();
	}
	/**
	 * 创建接收服务器数据的工具
	 针对消息
	 * @return [type] [description]
	 */
	public static function createServiceDataReceiveTool(){
		return new ServiceDataReceive();
	}
	/**
	 * 返回常用函数工具类
	 * @return [type] [description]
	 */
	public static function createFuncTool(){
		return new Func();
	}
	/**
	 * 创建用户管理类
	 * @return [type] [description]
	 */
	public static function createUserManage(){
		return new UserManage();
	}
}
