<?php 
namespace WeiChatLib;

/**
* 
*/
class ServiceDataReceive{
	/**
	 * 获取服务器发送过来的数据
	 * @param  [type] $type [description]
	 * @return [type]       [description]
	 */
	public function getGlobeContentToObj($type=Func::xmlData){
		$PostContent= $GLOBALS["HTTP_RAW_POST_DATA"];
// 		$PostContent=<<<XML
//     <xml><ToUserName><![CDATA[gh_9e1940c5ce7e]]></ToUserName>
// <FromUserName><![CDATA[oZWc3t4DwN6qQSaooL8nX5ixC95g]]></FromUserName>
// <CreateTime>1454146088</CreateTime>
// <MsgType><![CDATA[text]]></MsgType>
// <Content><![CDATA[阿斯顿发]]></Content>
// <MsgId>6245509891969057901</MsgId>
// </xml>
// XML;
		if(empty($PostContent)){
			return false;
		}
		$data=ToolFactory::createFuncTool()->stringDataToaArr($PostContent,$type);
		return (object)$data;
	}
}
