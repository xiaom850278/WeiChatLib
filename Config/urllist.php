<?php 
//由于access_token 在微信公众平台中有2个相同的所以把用户的access_token改成了access_token_user
return array(
		"getAccessToken"=>"https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET",


		//获取用户数据的
		"getUserCode"=>"https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE#wechat_redirect",

		"getUserAccessToken"=>"https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code",

		"refreshAccessToken"=>"https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=APPID&grant_type=refresh_token&refresh_token=REFRESH_TOKEN",

		"getUserInfo"=>"https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN_USER&openid=OPENID&lang=zh_CN",

		"checkAccessToken"=>"https://api.weixin.qq.com/sns/auth?access_token=ACCESS_TOKEN_USER&openid=OPENID",

		"getUserInfoByOpenid_Only"=>"https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN",

		"getUserInfoByListOpenid_Only"=>"https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=ACCESS_TOKEN",

		//用户分组
		"createUserGroup"=>"https://api.weixin.qq.com/cgi-bin/groups/create?access_token=ACCESS_TOKEN",

		"getAllGroup"=>"https://api.weixin.qq.com/cgi-bin/groups/get?access_token=ACCESS_TOKEN",

		"moveUserToGroup"=>"https://api.weixin.qq.com/cgi-bin/groups/members/batchupdate?access_token=ACCESS_TOKEN",

		"delectGroup"=>"https://api.weixin.qq.com/cgi-bin/groups/delete?access_token=ACCESS_TOKEN",

		"setUserNickname"=>"https://api.weixin.qq.com/cgi-bin/user/info/updateremark?access_token=ACCESS_TOKEN",

		"getUserListPage"=>"https://api.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&next_openid=NEXT_OPENID",
		
		//二维码
		"QRcode"=>"https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=ACCESS_TOKEN",
		"getImageUrl"=>"https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=TICKET",
		"longUrlToShort"=>"https://api.weixin.qq.com/cgi-bin/shorturl?access_token=ACCESS_TOKEN",
	);
